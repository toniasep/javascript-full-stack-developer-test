<?php

/* @var $this yii\web\View */

$this->title = 'Test 1';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Test 1</h1>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 mb-5">
                <h2>Problem 1</h2>
                <p data-sourcepos="26:4-26:148">Given a limited range of size <code>n</code> where array contains elements between <code>1</code> to <code>n-1</code> with one element repeating, find the duplicate number in it.</p>
                <p data-sourcepos="30:5-30:13">examples:</p>
                <pre lang="javascript" class="code highlight js-syntax-highlight language-javascript white" data-sourcepos="31:5-34:7"><code><span lang="javascript" class="line" id="LC1"><span class="p">[</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">3</span><span class="p">,</span><span class="mi">4</span><span class="p">,</span><span class="mi">4</span><span class="p">]</span></span>
<span lang="javascript" class="line" id="LC2"><span class="nx">the</span> <span class="nx">duplicate</span> <span class="nx">element</span> <span class="nx">is</span> <span class="mi">4</span></span></code></pre>
                <pre lang="javascript" class="code highlight js-syntax-highlight language-javascript white" data-sourcepos="36:5-39:7"><code><span lang="javascript" class="line" id="LC1"><span class="p">[</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">3</span><span class="p">,</span><span class="mi">4</span><span class="p">,</span><span class="mi">2</span><span class="p">]</span></span>
<span lang="javascript" class="line" id="LC2"><span class="nx">the</span> <span class="nx">duplicate</span> <span class="nx">element</span> <span class="nx">is</span> <span class="mi">2</span></span></code></pre>
                <a href="problem1" class="btn btn-primary">Problem 1 App</a>

            </div>
            <div class="col-lg-6 mb-5">
                <h2>Problem 2</h2>
                <p data-sourcepos="41:4-41:112">Given an array containing only <code>0</code>'s, <code>1</code>'s and <code>2</code>'s. sort the array in linear time and using constant space</p>
                <p data-sourcepos="30:5-30:13">examples:</p>
                <pre lang="javascript" class="code highlight js-syntax-highlight language-javascript white" data-sourcepos="46:5-49:7"><code><span lang="javascript" class="line" id="LC1"><span class="p">[</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">0</span><span class="p">]</span></span>
<span lang="javascript" class="line" id="LC2"><span class="p">[</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">2</span><span class="p">]</span></span></code></pre>
                <a href="problem2" class="btn btn-primary">Problem 2 App</a>

            </div>
            <div class="col-lg-6 mb-5">
                <h2>Problem 3</h2>
                <p data-sourcepos="51:4-51:116">Given an array of integers find all distinct combinations of given length where repetition of elements is allowed</p>
                <p data-sourcepos="30:5-30:13">examples:</p>
                <pre lang="javascript" class="code highlight js-syntax-highlight language-javascript white" data-sourcepos="56:5-59:7"><code><span lang="javascript" class="line" id="LC1"><span class="p">[</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">3</span><span class="p">]</span></span>
<span lang="javascript" class="line" id="LC2"><span class="p">[[</span><span class="mi">1</span><span class="p">,</span><span class="mi">1</span><span class="p">],</span> <span class="p">[</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">],</span> <span class="p">[</span><span class="mi">1</span><span class="p">,</span><span class="mi">3</span><span class="p">],</span> <span class="p">[</span><span class="mi">2</span><span class="p">,</span><span class="mi">2</span><span class="p">],</span> <span class="p">[</span><span class="mi">2</span><span class="p">,</span><span class="mi">3</span><span class="p">],</span> <span class="p">[</span><span class="mi">3</span><span class="p">,</span><span class="mi">3</span><span class="p">]]</span></span></code></pre>
                <a href="problem3" class="btn btn-primary">Problem 3 App</a>

            </div>
            <div class="col-lg-6 mb-5">
                <h2>Problem 4</h2>
                <p data-sourcepos="61:4-61:150">Given an array of integers, find maximum product (multiplication) subarray. In other words, find sub-array that has maximum product of its elements</p>
                <p data-sourcepos="30:5-30:13">examples:</p>
                <pre lang="javascript" class="code highlight js-syntax-highlight language-javascript white" data-sourcepos="66:5-69:7"><code><span lang="javascript" class="line" id="LC1"><span class="p">[</span><span class="o">-</span><span class="mi">6</span><span class="p">,</span><span class="mi">4</span><span class="p">,</span><span class="o">-</span><span class="mi">5</span><span class="p">,</span><span class="mi">8</span><span class="p">,</span><span class="o">-</span><span class="mi">10</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">8</span><span class="p">]</span></span>
<span lang="javascript" class="line" id="LC2"><span class="nx">the</span> <span class="nx">maximum</span> <span class="nx">product</span> <span class="nx">sub</span><span class="o">-</span><span class="nx">array</span> <span class="nx">is</span> <span class="p">[</span><span class="mi">4</span><span class="p">,</span><span class="o">-</span><span class="mi">5</span><span class="p">,</span><span class="mi">8</span><span class="p">,</span><span class="o">-</span><span class="mi">10</span><span class="p">]</span> <span class="nx">having</span> <span class="nx">product</span> <span class="mi">1600</span></span></code></pre>
                <a href="problem4" class="btn btn-primary">Problem 4 App</a>

            </div>
        </div>

    </div>
</div>