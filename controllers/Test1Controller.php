<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Data;

class Test1Controller extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
        
    }

    public function actionProblem1()
    {
        if (isset($_POST["Data"]["dataArray"])) {
            $data = $_POST["Data"]["dataArray"];
        }else{
            $data = '1,2,3,4,4';
        }
        $dataArray = explode(',', $data);
        $result = "no duplicate element";
        for ($i = 0; $i < count($dataArray); $i++) {
            for ($j = $i + 1; $j < count($dataArray); $j++) {
                if ($dataArray[$i] === $dataArray[$j]) {
                    $result = "the duplicate element is " . $dataArray[$i];
                    break;
                }
            }
        }
        $model = new Data();
        return $this->render('problem1', compact('model', 'result', 'data'));
    }

    public function actionProblem2()
    {
        if (isset($_POST["Data"]["dataArray"])) {
            $data = $_POST["Data"]["dataArray"];
        }else{
            $data = '0,1,2,2,1,0,0,2,0,1,1,0';
        }
        $dataArray = explode(',', $data);
        $start = 0;
        $mid = 0;
        $end = count($dataArray) - 1;
        $result = "";
        foreach ($dataArray as $number) {
            if ($dataArray[$mid] < 1) {
                $temp = $dataArray[$start];
                $dataArray[$start] = $dataArray[$mid];
                $dataArray[$mid] = $temp;
                $start += 1;
                $mid += 1;
            } elseif ($dataArray[$mid] > 1) {
                $temp = $dataArray[$mid];
                $dataArray[$mid] = $dataArray[$end];
                $dataArray[$end] = $temp;
                $end -= 1;
            } else {
                $mid += 1;
            }
        }
        for ($i = 0; $i < count($dataArray); $i++) {
            $result .= $dataArray[$i] . ",";
        }
        $result = rtrim($result, ", ");
        $model = new Data();
        return $this->render('problem2', compact('model', 'result', 'data'));
    }

    public function actionProblem3()
    {
        if (isset($_POST["Data"]["dataArray"])) {
            $data = $_POST["Data"]["dataArray"];
        }else{
            $data = '1,2,3';
        }
        
        $dataArray = explode(',', $data);

        $model = new Data();
        return $this->render('problem3', compact('model', 'dataArray', 'data'));
    }

    public function actionProblem4()
    {
        if (isset($_POST["Data"]["dataArray"])) {
            $data = $_POST["Data"]["dataArray"];
        } else {
            $data = '-6,4,-5,8,-10,0,8';
        }
        $dataArray = explode(',', $data);

        $max_ending = $dataArray[0];
        $min_ending = $dataArray[0];
        $max_so_far = $dataArray[0];

        for ($i=1; $i < count($dataArray); $i++) { 
            $temp = $max_ending;
            $max_ending = max($dataArray[$i], max($dataArray[$i] * $max_ending, $dataArray[$i] * $min_ending));

            $min_ending = min($dataArray[$i], min($dataArray[$i] * $temp, $dataArray[$i] * $min_ending));

            $max_so_far = max($max_so_far, $max_ending);
        }
        
        $result = $max_so_far;
        $model = new Data();
        return $this->render('problem4', compact('model', 'result', 'data'));

    }
        
    /**
     * Login action.
     *
     * @return Response|string
     */
    
}
