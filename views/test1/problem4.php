<?php

use yii\bootstrap4\ActiveForm;

$this->title = "Problem 4";
?>

<?php $form = ActiveForm::begin() ?>
<?php
if (isset($data)) {
    $model->dataArray = $data;
}
?>
<?= $form->field($model, "dataArray")->textInput() ?>

<button type="submit" class="btn btn-primary">Submit</button>

<?php $form::end() ?>

<?= "the maximum product sub-array is " . $result ?>