# How to Install

- Clone repository
- Create db lalu import file lussa_test.sql pada root repository 
- Sesuai koneksi database pada file '\config\db.php'


# How To Run

- Buka console arahkan ke direktori repo
- Ketik 'php yii serve' lalu enter
- Lalu pada console 'Server started on http://localhost:8080/'
- Buka http://localhost:8080/ pada browser
- Website akan mengarahkan pada halaman Test 1
- Ada 3 Menu yaitu Test 1, Test (2), CRUD 

# Test 1 

- Akses http://localhost:8080/test1/index
- Website akan menampilkan 4 Problem sesuai soal
- Dan ada Button di tiap problem, ketika di klik akan di arahkan ke halaman problem solve nya
- Ada input text untuk input/edit data, dan akan tampil data default sesuai soal
- Untuk setiap data nya pisahkan dengan koma (,)

# Test 2 (API Employee)

Gunakan Postman untuk melakukan request.
- Get Semua Employee

    Method GET, http://localhost:8080/employee
- Tambah Data Employee

    Method POST, http://localhost:8080/employee/create

    Body : 

        id : <id Employee>
        name : <nama Employee>
        email : <email Employee>
        role : <role Employee>

- Hapus Data Employe

    Method DELETE, http://localhost:8080/employee/delete

    Params : 

        id : <id Employee>

